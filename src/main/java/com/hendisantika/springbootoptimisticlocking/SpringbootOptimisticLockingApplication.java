package com.hendisantika.springbootoptimisticlocking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootOptimisticLockingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootOptimisticLockingApplication.class, args);
    }

}

