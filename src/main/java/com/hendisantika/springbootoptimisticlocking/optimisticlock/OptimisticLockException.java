package com.hendisantika.springbootoptimisticlocking.optimisticlock;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-optimistic-locking
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-20
 * Time: 09:00
 * To change this template use File | Settings | File Templates.
 */
public class OptimisticLockException extends RuntimeException {

    public OptimisticLockException(String description) {
        super(description);
    }
}