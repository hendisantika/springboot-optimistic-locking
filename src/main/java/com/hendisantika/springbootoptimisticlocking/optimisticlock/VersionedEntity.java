package com.hendisantika.springbootoptimisticlocking.optimisticlock;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-optimistic-locking
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-20
 * Time: 08:57
 * To change this template use File | Settings | File Templates.
 */
public interface VersionedEntity {
    Long getId();

    Long getVersion();

    void setVersion(Long version);

    String getTableName();
}