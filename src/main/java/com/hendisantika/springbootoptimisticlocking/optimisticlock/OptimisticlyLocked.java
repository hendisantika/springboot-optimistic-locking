package com.hendisantika.springbootoptimisticlocking.optimisticlock;

import java.lang.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-optimistic-locking
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-20
 * Time: 08:58
 * To change this template use File | Settings | File Templates.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface OptimisticlyLocked {
}
