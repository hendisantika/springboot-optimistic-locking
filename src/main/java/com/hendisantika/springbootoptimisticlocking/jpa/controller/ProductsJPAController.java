package com.hendisantika.springbootoptimisticlocking.jpa.controller;

import com.hendisantika.springbootoptimisticlocking.jpa.model.ProductJPA;
import com.hendisantika.springbootoptimisticlocking.jpa.repository.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-optimistic-locking
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-20
 * Time: 09:10
 * To change this template use File | Settings | File Templates.
 */

@RestController("JpaProductsController")
@RequestMapping("/jpa-products")
public class ProductsJPAController {
    @Autowired
    private ProductsRepository repo;


    @GetMapping
    public Iterable<ProductJPA> findAll() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Optional<ProductJPA> findOne(@PathVariable Long id) {
        return repo.findById(id);
    }

    @PutMapping
    public ProductJPA save(@RequestBody ProductJPA product) {
        return repo.save(product);
    }

}
