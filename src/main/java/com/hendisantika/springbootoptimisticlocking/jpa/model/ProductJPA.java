package com.hendisantika.springbootoptimisticlocking.jpa.model;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-optimistic-locking
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-20
 * Time: 09:08
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "product")
public class ProductJPA {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Version
    private Long version;

    private String name;

    private Long upc;

    protected ProductJPA() {
    }

    public ProductJPA(String name, Long upc) {
        this.name = name;
        this.upc = upc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUpc() {
        return upc;
    }

    public void setUpc(Long upc) {
        this.upc = upc;
    }
}
