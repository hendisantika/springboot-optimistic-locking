package com.hendisantika.springbootoptimisticlocking.jpa.repository;

import com.hendisantika.springbootoptimisticlocking.jpa.model.ProductJPA;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-optimistic-locking
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-20
 * Time: 09:09
 * To change this template use File | Settings | File Templates.
 */
public interface ProductsRepository extends CrudRepository<ProductJPA, Long> {
}
