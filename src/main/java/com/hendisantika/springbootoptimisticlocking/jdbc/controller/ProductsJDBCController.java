package com.hendisantika.springbootoptimisticlocking.jdbc.controller;

import com.hendisantika.springbootoptimisticlocking.jdbc.dao.ProductsDao;
import com.hendisantika.springbootoptimisticlocking.jdbc.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-optimistic-locking
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-20
 * Time: 09:06
 * To change this template use File | Settings | File Templates.
 */
@RestController("JdbcProductsController")
@RequestMapping("/jdbc-products")
public class ProductsJDBCController {
    @Autowired
    ProductsDao productsDao;

    @GetMapping
    public Product findAll() {
        return this.productsDao.findAll();
    }

    @GetMapping("/{id}")
    public Product findOne(@PathVariable Long id) {
        return this.productsDao.findOne(id);
    }

    @PutMapping
    public Product save(@RequestBody Product product) {
        return this.productsDao.save(product);
    }
}
